#include "qdir.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <string.h>
#include "gameconfig.h"
#include "ui_gameconfig.h"
#include <QDesktopServices>

Game::Game() {
    process = new QProcess;
    directory="";
    exec="";
    name="";
    runner="Wine";
    runnerver="";
    winepfx="";
    dxvk="none";
    vkd3d="none";
}

GameConfig::GameConfig(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GameConfig)
{
    ui->setupUi(this);

    confpath = qEnvironmentVariable("HOME")+"/.local/share/QtLaunch/";
    ui->envTable->setHorizontalHeaderLabels({"Key","Value"});
    defaultconf = loadGame("default.conf");
}
GameConfig::~GameConfig()
{
    delete ui;
    //delete fod;
}

void GameConfig::readRunnerDxvk(Game *confgame) {
    ui->runnerChoose->clear();
    ui->runnerChoose->insertItem(0, "Wine");
    ui->runnerChoose->insertItem(1, "Proton");
    ui->runnerChoose->setCurrentText(confgame->runner);
    QDir dir(winefolder);
    ui->wineChoose->clear();
    if(!confgame->runner.compare("Wine", Qt::CaseInsensitive)) {
        winever = dir.entryList(QDir::NoDotAndDotDot | QDir::Dirs);
        ui->wineChoose->insertItems(0, winever);
    } else if(!confgame->runner.compare("Proton", Qt::CaseInsensitive)) {
        dir.cd(protonfolder);
        protonver = dir.entryList(QDir::NoDotAndDotDot | QDir::Dirs);
        ui->wineChoose->insertItems(0, protonver);
    }
    ui->wineChoose->setCurrentText(confgame->runnerver);
    //ui->wineChoose->insertItem(0,"custom");
    //ui->wineChoose->insertItems(0, winever);
    dir.cd(dxvkfolder);
    dxvkver = dir.entryList(QDir::NoDotAndDotDot | QDir::Dirs);
    ui->dxvkChoose->clear();
    ui->dxvkChoose->insertItem(0,"none");
    ui->dxvkChoose->insertItems(1,dxvkver);
    ui->dxvkChoose->setCurrentText(confgame->dxvk);
    dir.cd(vkd3dfolder);
    vkd3dver = dir.entryList(QDir::NoDotAndDotDot | QDir::Dirs);
    ui->vkd3dChoose->clear();
    ui->vkd3dChoose->insertItem(0,"none");
    ui->vkd3dChoose->insertItems(1,vkd3dver);
    ui->vkd3dChoose->setCurrentText(confgame->vkd3d);
}


bool GameConfig::Config(Game *confgame) {
    size_t i;
    ui->nameEdit->setText(confgame->name);
    if(confgame->name == "default") ui->nameEdit->setReadOnly(true);
    else ui->nameEdit->setReadOnly(false);
    ui->dirEdit->setText(confgame->directory);
    ui->execEdit->setText(confgame->exec);
    ui->iconEdit->setText(confgame->icon);
    //ui->runnerChoose->setCurrentText(confgame->runner);
    //ui->wineChoose->setCurrentText(confgame->runnerver);
    readRunnerDxvk(confgame);
	ui->ulwglEdit->setText(confgame->ulwgl_id);
	ui->winepfxEdit->setText(confgame->winepfx);
	ui->cmdpfxEdit->setText(confgame->cmdpfx);
    ui->argEdit->setText(confgame->args);
    ui->dxvkChoose->setCurrentText(confgame->dxvk);
    ui->vkd3dChoose->setCurrentText(confgame->vkd3d);
    ui->envTable->clearContents();
    ui->envTable->setRowCount(confgame->env.size());
    for (i=0; i<confgame->env.size(); i++) {
        QTableWidgetItem *item = new QTableWidgetItem(confgame->env.at(i).name);
        ui->envTable->setItem(i, 0, item);
        item = new QTableWidgetItem(confgame->env.at(i).value);
        ui->envTable->setItem(i, 1, item);
    }
    if(this->exec() == QDialog::Accepted) {
        confgame->name = ui->nameEdit->text();
        confgame->directory = ui->dirEdit->text();
        confgame->exec = ui->execEdit->text();
        confgame->args = ui->argEdit->text();
        confgame->icon = ui->iconEdit->text();
        confgame->runner = ui->runnerChoose->currentText();
        confgame->runnerver = ui->wineChoose->currentText();
		confgame->ulwgl_id = ui->ulwglEdit->text();
		confgame->winepfx = ui->winepfxEdit->text();
		confgame->cmdpfx =  ui->cmdpfxEdit->text();
        confgame->dxvk = ui->dxvkChoose->currentText();
        confgame->vkd3d = ui->vkd3dChoose->currentText();
        confgame->env.clear();
        for (i=0; i<(size_t)ui->envTable->rowCount(); i++) {
            env_entry entry;
            entry.name = ui->envTable->item(i, 0)->text();
            entry.value = ui->envTable->item(i, 1)->text();
            confgame->env.emplace_back(entry);
        }
        saveGame(confgame);
        return true;
    } else
        return false;
}

void GameConfig::saveGame(Game *game) {
    std::string filename = confpath.toStdString();
    filename += "games/"+game->name.toStdString()+".conf";
    std::cout << "saving to " << filename << std::endl;
    std::ofstream gamefile(filename);
    if (!gamefile.good()) {
        std::cerr << "Error opening file" << std::endl;
        return;
    }
    gamefile << "directory=" << game->directory.toStdString() << std::endl;
    gamefile << "exec=" << game->exec.toStdString()  << std::endl;
    gamefile << "args=" << game->args.toStdString()  << std::endl;
    gamefile << "icon=" << game->icon.toStdString()  << std::endl;
    gamefile << "runner=" << game->runner.toStdString()  << std::endl;
    gamefile << "runnerver=" << game->runnerver.toStdString()  << std::endl;
    gamefile << "ulwgl_id=" << game->ulwgl_id.toStdString()  << std::endl;
    gamefile << "winepfx=" << game->winepfx.toStdString()  << std::endl;
    gamefile << "cmdpfx=" << game->cmdpfx.toStdString()  << std::endl;
    gamefile << "dxvk=" << game->dxvk.toStdString()  << std::endl;
    gamefile << "vkd3d=" << game->vkd3d.toStdString()  << std::endl;
    gamefile << "environment:" << std::endl;
    for (size_t i=0; i<game->env.size(); i++) {
        gamefile << game->env.at(i).name.toStdString() << "=" << game->env.at(i).value.toStdString() << std::endl;
    }
    gamefile.close();
}

Game *GameConfig::loadGame(std::string name) {
    Game *game = new Game;
    std::string filename = confpath.toStdString();
    filename += "games/"+name;
    std::cout << "loading " << filename << std::endl;
    std::ifstream gamefile(filename);
    if (!gamefile.good()) {
        std::cerr << "Error opening file" << std::endl;
        return game;
    }
    int pos = name.find(".conf");
    game->name = name.erase(pos, pos+5).c_str();

    std::string buffer;
    while (getline(gamefile, buffer)) {
        if(buffer.substr(0,9) == "directory") {
            game->directory = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
        } else if(buffer.substr(0,4) == "exec") {
            game->exec = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
        } else if(buffer.substr(0,4) == "args") {
            game->args = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
        } else if(buffer.substr(0,4) == "icon") {
            game->icon = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
        } else if(buffer.substr(0,9) == "runnerver") {
            game->runnerver = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
        } else if(buffer.substr(0,6) == "runner") {
            game->runner = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
        } else if(buffer.substr(0,8) == "ulwgl_id") {
            game->ulwgl_id = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
        } else if(buffer.substr(0,7) == "winepfx") {
            game->winepfx = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
        } else if(buffer.substr(0,6) == "cmdpfx") {
            game->cmdpfx = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
        } else if(buffer.substr(0,4) == "dxvk") {
            game->dxvk = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
        } else if(buffer.substr(0,5) == "vkd3d") {
            game->vkd3d = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
        } else if(buffer.substr(0,12) == "environment:") {
            while (getline(gamefile, buffer)) {
                env_entry entry;
                int pos = buffer.find('=');
                entry.name = QString::fromStdString(buffer.substr(0, pos));
                entry.value = QString::fromStdString(buffer.substr(pos+1, buffer.size()));
                game->env.emplace_back(entry);
            }
        }
    }
    gamefile.close();

    return game;
}

void GameConfig::loadGames(std::vector<Game> *games, QListWidget *gameList) {
    Game *game;
    QIcon defaulticon(":/resources/icon.png");

    QDir dir(confpath+"games");
    foreach (QString file, dir.entryList(QDir::Files)) {
        if(file == "default.conf") continue;
        game = loadGame(file.toStdString());
        games->emplace_back(*game);

        QListWidgetItem *item = new QListWidgetItem;
        item->setText(game->name);

        item->setIcon(defaulticon);
        if (!game->icon.isEmpty()) {
            item->setIcon(QIcon(game->icon));
        }
        /*dir.cd(confpath+"icons");
        foreach (QString icon, dir.entryList(QStringList() << "*.png", QDir::Files)) {
            if (icon.startsWith(game->name)) {
                item->setIcon(QIcon(confpath+"icons/"+icon));
            }
        }*/

        gameList->addItem(item);
    }
}

void GameConfig::savePlayed(std::vector<Game> *games) {
    std::string filename = confpath.toStdString()+"time";
    QTime time(0,0,0);

    std::ofstream timefile(filename);
    if (!timefile.is_open()) {
        std::cerr << "Error saving timefile" << std::endl;
        return;
    }
    for(size_t i=0; i<games->size(); i++) {
        timefile << games->at(i).name.toStdString() << ": " << games->at(i).playhour << " "
                 << games->at(i).playmin << " " << games->at(i).playsec << std::endl;
    }
    timefile.close();
}
void GameConfig::loadPlayed(std::vector<Game> *games) {
    std::string name, filename = confpath.toStdString()+"time";
	int hour = 0,min = 0, sec = 0;

	std::ifstream timefile(filename);
    if (!timefile.is_open()) {
        std::cerr << "Error loading timefile" << std::endl;
        return;
    }
    std::string buffer;
    while (getline(timefile, buffer)) {
        for(size_t i=0; i<games->size(); i++) {
            int pos=buffer.find(':');
            name = buffer.substr(0, pos);
            sscanf(buffer.substr(pos+1, buffer.npos).c_str(), "%d %d %d", &hour, &min, &sec);
            //seconds = stoi(buffer.substr(pos+1, buffer.npos));
            if(name == games->at(i).name.toStdString()) {
				games->at(i).playhour = hour;
				games->at(i).playmin = min;
				games->at(i).playsec = sec;
				std::cout << games->at(i).name.toStdString() << ": " << hour << " hours " << min << " minutes " << sec << " seconds\n";
			}
		}
    }
    timefile.close();
}

void GameConfig::on_execBrowse_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, "Select executable", ui->execEdit->text());
    if(!filename.isEmpty())
        ui->execEdit->setText(filename);
}
void GameConfig::on_winepfxBrowse_clicked()
{
    QString dirname = QFileDialog::getExistingDirectory(this, "Select wineprefix", ui->winepfxEdit->text(), QFileDialog::ShowDirsOnly);
    if(!dirname.isEmpty())
        ui->winepfxEdit->setText(dirname);
}
void GameConfig::on_dirBrowse_clicked()
{
    QString dirname = QFileDialog::getExistingDirectory(this, "Select directory", ui->dirEdit->text(), QFileDialog::ShowDirsOnly);
    if(!dirname.isEmpty())
        ui->dirEdit->setText(dirname);
}
void GameConfig::on_iconBrowse_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, "Select executable", ui->iconEdit->text());
    if(!filename.isEmpty())
        ui->iconEdit->setText(filename);
}


void GameConfig::on_envAddButton_clicked()
{
    ui->envTable->setRowCount(ui->envTable->rowCount()+1);
}


void GameConfig::on_envDelButton_clicked()
{
    ui->envTable->removeRow(ui->envTable->currentRow());
}

void GameConfig::on_runnerChoose_textActivated(const QString &arg1)
{
    if (this->game && !arg1.isEmpty() && this->game->runner.compare(arg1, Qt::CaseInsensitive)) {
        this->game->runner = arg1;
        readRunnerDxvk(this->game);
    }
}


void GameConfig::on_ulwglSearch_clicked()
{
    QDesktopServices::openUrl(QUrl("https://ulwgl.openwinecomponents.org/index.php"));
}
