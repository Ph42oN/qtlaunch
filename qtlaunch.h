#ifndef QTLAUNCH_H
#define QTLAUNCH_H

#include "gameconfig.h"
#include <QWidget>
#include "launcherconfig.h"
#include "qmenu.h"
#include "ui_qtlaunch.h"
#include <QMessageBox>
#include "ui_gamelog.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QtLaunch; }
QT_END_NAMESPACE

class QtLaunch : public QWidget
{
    Q_OBJECT

public:
    QtLaunch(QWidget *parent = nullptr);
    ~QtLaunch();

    std::vector<Game> games;
    LauncherConfig launchconf;
    GameConfig gameconf;
	QMenu winemenu;
	void setGamelistSize(int iconsize);

private slots:
    void on_addButton_clicked();

    void on_playstopButton_clicked();

    void on_gamesettingButton_clicked();

    void onStateChanged();
    void onGameStopped();
    void showPlayed(int game);
    void startGame(int game);
    void stopGame(int game);
    bool gameRunning(int game);
    int findGame(QString name);
    void gameLog();
    void gameLogErr();
    void checkdxvk(int game);
    void checkvkd3d(int game);
    void restoredlls(int game, QStringList dlls);
    void replacedlls(int game, QStringList dlls, QStringList dllpaths);
    bool symlinkdll(QString dll, QString pfxpath, QString dllpath);

    void winecfg();
    void winereg();
    void wineconsole();
    void wineexe();
    void winetricks();
    void winerun(int game, QStringList args);


    void on_gameList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

    void on_settingButton_clicked();
    void on_logButton_clicked();
    void on_wineButton_clicked();

private:
    Ui::QtLaunch *ui;
    QMessageBox message;
    QDialog logDialog;
    Ui::GameLog logUi;
};

#endif // QTLAUNCH_H
