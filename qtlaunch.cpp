#include "qtlaunch.h"
#include <cstdlib>
#include <QProcess>
#include <QtConcurrent/QtConcurrent>
#include <QScrollBar>
#include <QMenu>
#include <filesystem>
#include <string>
#include <iostream>
#include <fstream>
#include <signal.h>
#include <thread>
#include <QFileSystemModel>

QtLaunch::QtLaunch(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::QtLaunch)
{
    ui->setupUi(this);

    launchconf.gameconf = &gameconf;
    launchconf.launcher = this;
    launchconf.init();

    gameconf.loadGames(&games, ui->gameList);
    gameconf.loadPlayed(&games);
    winemenu.addAction(tr("Winecfg"), this, SLOT(winecfg()));
    winemenu.addAction(tr("Wine regedit"), this, SLOT(winereg()));
    winemenu.addAction(tr("Wine console"), this, SLOT(wineconsole()));
    winemenu.addAction(tr("Winetricks"), this, SLOT(winetricks()));
    winemenu.addAction(tr("Run exe in wineprefix"), this, SLOT(wineexe()));

    logUi.setupUi(&logDialog);

    setGamelistSize(launchconf.iconsize);
}

QtLaunch::~QtLaunch()
{
    delete ui;
}


void QtLaunch::on_addButton_clicked()
{
    Game game = *gameconf.defaultconf;
    game.name = "";

    if(gameconf.Config(&game)) { //edit empty game
        games.emplace_back(game);

        QDir dir(game.directory);
        if (!game.directory.isEmpty() && !dir.exists()) {
			dir.mkpath(game.directory);
		}

		QListWidgetItem *item = new QListWidgetItem;
        item->setText(game.name);

        item->setIcon(QIcon(":/resources/icon.png"));
        if (!game.icon.isEmpty()) {
            item->setIcon(QIcon(game.icon));
        }

		ui->gameList->insertItem(0, item);
        int gamenum=findGame(game.name);
        winerun(gamenum, {"wineboot"});
        checkdxvk(gamenum);
        checkvkd3d(gamenum);
    }
}


void QtLaunch::on_playstopButton_clicked()
{
    int game = findGame("currentItem");
    if(game > -1) {
        if(ui->playstopButton->text()=="Stop") {
            stopGame(game);
        } else {
            //std::cout << "starting " << games[i].name.toStdString() << "\n";
            startGame(game);
        }
    }
}

bool QtLaunch::gameRunning(int game) {
    if(games[game].process->state() == QProcess::NotRunning) {
        return false;
    } else {
        return true;
    }
}

void QtLaunch::startGame(int game) {
	QStringList args = {games[game].exec};
	args.append(games[game].args.split(" "));
	winerun(game, args);

	logUi.textEdit->clear();
    games[game].gamelog = "";
    ui->playstopButton->setText("Stop");
    //games[game].process->start();
    QDateTime time;
    games[game].runtime = time.currentDateTime();

    connect(games[game].process, SIGNAL(finished(int)), this, SLOT(onGameStopped()));
    connect(games[game].process, &QProcess::stateChanged, this, &QtLaunch::onStateChanged);
    connect(games[game].process, SIGNAL(readyReadStandardOutput()), this, SLOT(gameLog()));
    connect(games[game].process, SIGNAL(readyReadStandardError()), this, SLOT(gameLogErr()));
}

void QtLaunch::gameLog() {
    int game = findGame("currentItem");
    if(game > -1) {
        QString text;
        while (games[game].process->canReadLine()) {
            text += games[game].process->readLine();
        }
        logUi.textEdit->insertPlainText(text);
        games[game].gamelog += text;
        QScrollBar *vert = logUi.textEdit->verticalScrollBar();
        vert->setValue(vert->maximum());
        return;
    }
}
void QtLaunch::gameLogErr() {
    int game = findGame("currentItem");
    if(game > -1) {
        QString text;
        while (games[game].process->canReadLine()) {
            text += games[game].process->readLine();
        }
        logUi.textEdit->insertHtml("<font color=\"red\">"+text+"</font>");
        games[game].gamelog += text;
        QScrollBar *vert = logUi.textEdit->verticalScrollBar();
        vert->setValue(vert->maximum());
        return;
    }
}

void QtLaunch::onGameStopped() {
    int game = findGame("currentItem");
    if(game > -1) {
        QDateTime time; //calculate playtime
		long secs = games[game].runtime.secsTo(time.currentDateTime());
        int hours = secs / 60 / 60;
		secs -= hours * 60 * 60;
        int min = secs / 60;
		secs -= min * 60;
        secs += games[game].playsec;
        while (secs >=60) {
			min++;
			secs -= 60;
		}
		games[game].playsec = secs;
		min += games[game].playmin;
		while (min >= 60) {
			hours++;
			min -= 60;
		}
		games[game].playmin = min;
		games[game].playhour += hours;
		games[game].runtime = time.currentDateTime(); //prevent time from getting added twice
        showPlayed(game);
        gameconf.savePlayed(&games);
    }
}

void QtLaunch::onStateChanged() {
    int game = findGame("currentItem");
    if(game > -1) {
        if(games[game].process->state() != QProcess::NotRunning) {
            ui->playstopButton->setText("Stop");
        } else {
            ui->playstopButton->setText("Play");
        }
        return;
    }
}
void QtLaunch::showPlayed(int game) {
    std::string played;
    if (games[game].playhour > 0) {
		played = std::to_string(games[game].playhour) + " hours ";
	}
	played += std::to_string(games[game].playmin) + " minutes";
	ui->timePlayed->setText(QString::fromStdString(played));
	std::cout << played;
}

void QtLaunch::stopGame(int game) {
    ui->playstopButton->setText("Play");
    std::string cmd = "WINEPREFIX="+games[game].winepfx.toStdString()+" ";
    if(games[game].runner == "Wine") {
        //cmd += gameconf.winefolder.toStdString()+"/" + games[game].runnerver.toStdString()+"/bin/wineserver -k";
        winerun(game, {"wineboot", "-k"});
    } else if(games[game].runner == "Proton") {
        cmd += gameconf.protonfolder.toStdString()+"/" + games[game].runnerver.toStdString()+"/files/bin/wineserver -k";
        system(cmd.c_str());
    }
    //system(cmd.c_str());
    games[game].process->close();
}

int QtLaunch::findGame(QString name) {
    if (name.isEmpty()) return -1;
    if (name == "currentItem" && ui->gameList->currentItem())
        name = ui->gameList->currentItem()->text();
    for(size_t i=0; i < games.size(); i++) {
        if(name == games[i].name) {
            return i;
        }
    }
    return -1;
}


void QtLaunch::on_gamesettingButton_clicked()
{
    int game = findGame("currentItem");
    if(game > -1) {
        QString winetemp = games[game].runnerver, icontemp=games[game].icon;
        if (gameconf.Config(&games[game])) {
            if(games[game].runnerver != winetemp)
                winerun(game, {"wineboot"});
            if(games[game].icon != icontemp)
                ui->gameList->currentItem()->setIcon(QIcon(games[game].icon));
            checkdxvk(game);
            checkvkd3d(game);
        }
    }
}


void QtLaunch::on_gameList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    int game = findGame(current->text());
    if(game > -1) {
        ui->gameName->setText("<p style=\"font-size:12pt;\">"+current->text()+"</p>");
        showPlayed(game);
        onStateChanged();
        launchconf.gameconf->game = &games[game];
	}
}

void QtLaunch::checkdxvk(int game) {
    QStringList dxvk_dlls = {"d3d10core.dll","d3d11.dll", "d3d9.dll", "dxgi.dll"};
    QStringList dxvkpath = { gameconf.dxvkfolder+"/"+games[game].dxvk+"/x64/",
                             gameconf.dxvkfolder+"/"+games[game].dxvk+"/x32/"};

    if(games[game].dxvk=="none") {
        restoredlls(game, dxvk_dlls);
    } else  {
        replacedlls(game, dxvk_dlls, dxvkpath);
    }
}
void QtLaunch::checkvkd3d(int game) {
    QStringList vkd3d_dlls = {"d3d12core.dll","d3d12.dll"};
    QStringList vkd3dpath = { gameconf.vkd3dfolder+"/"+games[game].vkd3d+"/x64/",
                             gameconf.vkd3dfolder+"/"+games[game].vkd3d+"/x86/"};

    if(games[game].vkd3d=="none") {
        restoredlls(game, vkd3d_dlls);
    } else  {
        replacedlls(game, vkd3d_dlls, vkd3dpath);
    }
}
void QtLaunch::restoredlls(int game, QStringList dlls) {
    QStringList pfxpath = { games[game].winepfx+"/drive_c/windows/system32/",
                            games[game].winepfx+"/drive_c/windows/syswow64/"};
    QString cmd;
    foreach (QString file, dlls) {
        bool restored = false;
        for(int i=0; i<2; i++) {
            QDir dir(pfxpath.at(i));
            QFileInfo cur_dll(pfxpath.at(i)+file);
            if (cur_dll.isSymLink()) {
                dir.remove(file);
                restored = dir.rename(file+".orig", file);
            }
        }
        if (restored) {
            cmd="reg delete \"HKEY_CURRENT_USER\\Software\\Wine\\DllOverrides\" /v "
                    +file.left(file.size()-4)+" /f";
			std::cout << "Running: " << cmd.toStdString() << std::endl;
			winerun(game, {cmd});
		}
    }
}
void QtLaunch::replacedlls(int game, QStringList dlls, QStringList dllpaths) {
    QStringList pfxpath = { games[game].winepfx+"/drive_c/windows/system32/",
                            games[game].winepfx+"/drive_c/windows/syswow64/"};
    QString cmd;
    foreach (QString file, dlls) {
        bool linked;
        for(int i=0; i<2; i++) {
            linked = symlinkdll(file, pfxpath.at(i), dllpaths.at(i));
        }
        if(linked) {
            cmd="reg add \"HKEY_CURRENT_USER\\Software\\Wine\\DllOverrides\" /v "
                    +file.left(file.size()-4)+" /d native /f";
            std::cout << "Running: " << cmd.toStdString() << std::endl;
            winerun(game, {cmd});
        }
    }
}
bool QtLaunch::symlinkdll(QString dll, QString pfxpath, QString dllpath) {
    QDir dir(pfxpath);
    QFileInfo cur_dll(pfxpath+dll);
    if (cur_dll.symLinkTarget() == dllpath+dll) return false;
    if (cur_dll.isSymLink()) {
        dir.remove(dll);
    } else {
        if(!dir.rename(dll, dll+".orig"))
            std::cerr << pfxpath.toStdString()+dll.toStdString()+" rename failed\n";
    }
    QFile file(dllpath+dll);
    if(!file.link(pfxpath+dll)) {
        std::cerr << dllpath.toStdString()+dll.toStdString()+" symlink failed\n";
        return false;
    }
    return true;
}

void QtLaunch::winecfg() {
    int game = findGame("currentItem");
    if(game > -1) {
        winerun(game, {"winecfg"});
    }
}
void QtLaunch::winereg() {
    int game = findGame("currentItem");
    if(game > -1) {
        winerun(game, {"regedit"});
    }
}
void QtLaunch::wineconsole() {
    int game = findGame("currentItem");
    if(game > -1) {
        winerun(game, {"wineconsole"});
    }
}
void QtLaunch::wineexe() {
    int game = findGame("currentItem");
    if(game > -1) {
        QString filename = QFileDialog::getOpenFileName(this, "Select executable", games[game].directory);
        if(!filename.isEmpty())
            winerun(game, {filename});
    }
}

void QtLaunch::winerun(int game, QStringList args) {
	QProcess *process;
	if (args.at(0) == games[game].exec) {
		process = games[game].process;
	} else {
		process = new QProcess;
	}
	QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
	env.insert("WINEPREFIX", games[game].winepfx);
    env.insert("CMDPFX", games[game].cmdpfx);
    for (size_t i=0; i<games[game].env.size(); i++) {
        env.insert(games[game].env.at(i).name, games[game].env.at(i).value);
    }
    //QStringList wineargs;
	if (games[game].runner == "Wine") {
		env.insert("WINEPATH", gameconf.winefolder+"/"+games[game].runnerver+"/bin/wine");
        //process->setWorkingDirectory(games[game].directory);
        process->setProgram(launchconf.confpath+"winerun");
	} else if (games[game].runner == "Proton") {
        //wineargs.append(gameconf.protonfolder+"/"+games[game].runnerver);
        env.insert("PROTONPATH", gameconf.protonfolder+"/"+games[game].runnerver);
        env.insert("GAMEID", games[game].ulwgl_id);
        process->setProgram(gameconf.ulwglfolder+"/ulwgl-run");
	}
    //wineargs.append(args);
	process->setProcessEnvironment(env);
    process->setWorkingDirectory(games[game].directory);
    process->setArguments(args);
    process->setProcessChannelMode(QProcess::ProcessChannelMode::MergedChannels);
    process->setReadChannel(QProcess::StandardOutput);
    process->start();

    if (process != games[game].process) {
		process->waitForFinished(-1);
		process->close();
		delete process;
	}
}

void QtLaunch::winetricks() {
    int game = findGame("currentItem");
    if(game != -1) {
        std::string cmd = "WINEPREFIX='"+games[game].winepfx.toStdString();
        if(games[game].runner == "Wine")
            cmd += "' WINE='"+gameconf.winefolder.toStdString()+"/" + games[game].runnerver.toStdString()+"/bin/wine";
        else if(games[game].runner == "Proton")
            cmd += "' WINE='"+gameconf.protonfolder.toStdString()+"/" + games[game].runnerver.toStdString()+"/files/bin/wine";
        cmd += "' winetricks";
        system(cmd.c_str());
    }
}

void QtLaunch::on_wineButton_clicked()
{
    QPoint point = ui->wineButton->pos();
    point.setY(point.y()+35);
    point.setX(point.x());
    winemenu.exec(mapToGlobal(point));
}
void QtLaunch::on_settingButton_clicked()
{
    launchconf.config();
}
void QtLaunch::on_logButton_clicked()
{
    logDialog.show();
}

void QtLaunch::setGamelistSize(int iconsize) {
    ui->gameList->setIconSize(QSize(iconsize, iconsize));
    QFont font;
    font.setPixelSize(iconsize/3+6);
    ui->gameList->setFont(font);
}
