#ifndef LAUNCHERCONFIG_H
#define LAUNCHERCONFIG_H

#include <QDialog>
#include "gameconfig.h"
#include <QMenu>
#include <QSystemTrayIcon>
#include "ui_process.h"

struct nextProcess {
	QString cmd;
	QStringList args;
};

#pragma once
class QtLaunch;

namespace Ui {
class LauncherConfig;
}

class LauncherConfig : public QDialog
{
    Q_OBJECT

public:
    explicit LauncherConfig(QWidget *parent = nullptr);
    ~LauncherConfig();

    QString confpath;
    GameConfig *gameconf;
    QtLaunch *launcher;
	struct nextProcess nextProcess;

	bool enabletray = true;
    int iconsize = 32;

    void init();
    void config();
    void loadConfig();
	void downloadfile(char *url);
	void checkulwgl(QString ulwglfolder, QString confpath);
	void runProcess(QString process, QStringList args);

private slots:
    void on_winefButton_clicked();
    void on_protonfButton_clicked();
    void on_dxvkfButton_clicked();
    void on_vkd3dfButton_clicked();
    void on_ulwglfButton_clicked();
    void on_defconfButton_clicked();

    void on_buttonBox_accepted();
    void trayClicked(QSystemTrayIcon::ActivationReason reason_);
    void trayShowHide();

    void processFinished(int exitcode);
	void processOut();
	void processErr();

private:
    QSystemTrayIcon tray;
    QMenu traymenu;
    Ui::LauncherConfig *ui;
    QDialog processDialog;
    Ui::ProcessUI processUI;
    QProcess *process;

};

#endif // LAUNCHERCONFIG_H
