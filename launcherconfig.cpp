#include "launcherconfig.h"
#include "qtlaunch.h"
#include "ui_launcherconfig.h"
#include <QFileDialog>
#include <QScrollBar>
#include <QtConcurrent>
#include <fstream>
#include <iostream>
#include <thread>

void toggleVisible();

LauncherConfig::LauncherConfig(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LauncherConfig)
{
    ui->setupUi(this);

    confpath = qEnvironmentVariable("HOME")+"/.local/share/QtLaunch/";

    tray.setIcon(QIcon(":/resources/icon.png"));
    connect(&tray, &QSystemTrayIcon::activated, this, &LauncherConfig::trayClicked);

    QAction *quitAction = new QAction("&Quit", this);
    connect(quitAction, &QAction::triggered, qApp, &QCoreApplication::quit);
    traymenu.addAction("Show / Hide", this, SLOT(trayShowHide()));
    traymenu.addAction(quitAction);
    tray.setContextMenu(&traymenu);

	processUI.setupUi(&processDialog);
}

LauncherConfig::~LauncherConfig()
{
    delete ui;
}

void LauncherConfig::init() {
    loadConfig();

    QDir dir(confpath);
    if (!dir.exists()) {
        dir.mkpath(confpath+"games");
        dir.mkpath(confpath+"icons");
    }

    std::string filename = confpath.toStdString()+"winerun";
    std::fstream file;
    file.open(filename,std::ios::in);
    if (!file.is_open()) {
        file.open(filename,std::ios::out);
        file << "#!/bin/bash\n$CMDPFX $WINEPATH \"$@\" &&\n${WINEPATH}server -w";
    }
    file.close();

    if (enabletray) {
        tray.show();
    }
}

void LauncherConfig::config() {
    ui->winefEdit->setText(gameconf->winefolder);
    ui->protonfEdit->setText(gameconf->protonfolder);
    ui->dxvkfEdit->setText(gameconf->dxvkfolder);
    ui->vkd3dfEdit->setText(gameconf->vkd3dfolder);
	ui->ulwglfEdit->setText(gameconf->ulwglfolder);
	ui->trayCheckBox->setChecked(enabletray);
	ui->chooseIconSize->setCurrentText(QString::number(iconsize));
    this->open();
}

void LauncherConfig::on_winefButton_clicked()
{
    QString dirname = QFileDialog::getExistingDirectory(this, "Select directory", ui->winefEdit->text(), QFileDialog::ShowDirsOnly);
    if(!dirname.isEmpty())
        ui->winefEdit->setText(dirname);
}
void LauncherConfig::on_protonfButton_clicked()
{
    QString dirname = QFileDialog::getExistingDirectory(this, "Select directory", ui->protonfEdit->text(), QFileDialog::ShowDirsOnly);
    if(!dirname.isEmpty())
        ui->protonfEdit->setText(dirname);
}
void LauncherConfig::on_dxvkfButton_clicked()
{
    QString dirname = QFileDialog::getExistingDirectory(this, "Select directory", ui->dxvkfEdit->text(), QFileDialog::ShowDirsOnly);
    if(!dirname.isEmpty())
        ui->dxvkfEdit->setText(dirname);
}
void LauncherConfig::on_vkd3dfButton_clicked()
{
    QString dirname = QFileDialog::getExistingDirectory(this, "Select directory", ui->vkd3dfEdit->text(), QFileDialog::ShowDirsOnly);
    if(!dirname.isEmpty())
        ui->vkd3dfEdit->setText(dirname);
}
void LauncherConfig::on_ulwglfButton_clicked()
{
    QString dirname = QFileDialog::getExistingDirectory(this, "Select directory", ui->ulwglfEdit->text(), QFileDialog::ShowDirsOnly);
    if(!dirname.isEmpty())
        ui->ulwglfEdit->setText(dirname);
}
void LauncherConfig::on_defconfButton_clicked()
{
    gameconf->defaultconf->name="default";
    gameconf->Config(gameconf->defaultconf);
}

void LauncherConfig::loadConfig() {
    std::ifstream file(confpath.toStdString()+"launcher.conf");
    if(!file.good()) {
        //std::cout << "Using default wine and dxvk folders\n";
        gameconf->winefolder = confpath+"wine";
        gameconf->protonfolder = qEnvironmentVariable("HOME")+"/.local/share/Steam/compatibilitytools.d";
        gameconf->dxvkfolder = confpath+"dxvk";
        gameconf->vkd3dfolder = confpath+"vkd3d";
        gameconf->ulwglfolder = qEnvironmentVariable("HOME")+"/.local/share/ULWGL";
        return;
    }
    std::string buffer;
    while (getline(file, buffer)) {
        if(buffer.substr(0,10) == "winefolder") {
            gameconf->winefolder = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
        } else if(buffer.substr(0,12) == "protonfolder") {
            gameconf->protonfolder = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
        } else if(buffer.substr(0,10) == "dxvkfolder") {
            gameconf->dxvkfolder = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
        } else if(buffer.substr(0,11) == "vkd3dfolder") {
            gameconf->vkd3dfolder = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
        } else if(buffer.substr(0,11) == "ulwglfolder") {
            gameconf->ulwglfolder = buffer.substr(buffer.find('=')+1, buffer.npos).c_str();
        } else if(buffer.substr(0,4) == "tray") {
            if (buffer.substr(buffer.find('=')+1, buffer.npos) == "false")
                enabletray = false;
            else
                enabletray = true;
        } else if(buffer.substr(0,8) == "iconsize") {
            iconsize = std::stoi(buffer.substr(buffer.find('=')+1, buffer.npos));
        }
    }
    file.close();
}

void LauncherConfig::downloadfile(char *url) {
    QDir dir(confpath+"download");
    if (!dir.exists()) {
        dir.mkpath(confpath+"download");
    }
	runProcess("wget", {"-P", confpath + "download", url});
    //processDialog.setWindowTitle("downloading...");
	//return waitProcess();
}
void LauncherConfig::runProcess(QString cmd, QStringList args) {
	process = new QProcess;
	process->setProgram(cmd);
	process->setArguments(args);
	process->setProcessChannelMode(QProcess::MergedChannels);
    process->setReadChannel(QProcess::StandardOutput);
	processDialog.setWindowTitle("Running " + cmd);
	processDialog.show();

	connect(process, SIGNAL(readyReadStandardOutput()), this, SLOT(processOut()));
	connect(process, SIGNAL(readyReadStandardError()), this, SLOT(processErr()));
	connect(process, &QProcess::finished, this, &LauncherConfig::processFinished);
	process->start();
}

void LauncherConfig::processFinished(int exitcode) {
	if (exitcode == 0) {
		processDialog.hide();
	} else {
        processUI.textEdit->insertHtml("<font color=\"red\">Error running "+ process->program()+" "+process->arguments().join(" ") +"</font>");
	}
	process->close();
	delete process;
    if (!exitcode && !nextProcess.cmd.isEmpty()) {
		runProcess(nextProcess.cmd, nextProcess.args);
	}
    nextProcess.cmd.clear();
}
void LauncherConfig::processOut() {
	QString text;
	while (process->canReadLine()) {
		text += process->readLine();
	}
	
	processUI.textEdit->insertPlainText(text);
    QScrollBar *vert = processUI.textEdit->verticalScrollBar();
        vert->setValue(vert->maximum());
	return;
}
void LauncherConfig::processErr() {
	QString text;
    while(process->canReadLine()) {
		text += process->readLine();
	}
	processUI.textEdit->insertHtml("<font color=\"red\">" + text + "</font>");
    QScrollBar *vert = processUI.textEdit->verticalScrollBar();
        vert->setValue(vert->maximum());
	return;
}

void LauncherConfig::checkulwgl(QString ulwglfolder, QString confpath) {
    QFileInfo file(ulwglfolder + "/ulwgl-run");
	QDir dir(ulwglfolder);
	QString ulwgltar = confpath + "download/ULWGL-launcher.tar.gz";
	if (!file.exists() || !dir.exists()) {
		dir.mkpath(ulwglfolder);
		file.setFile(ulwgltar);
		if(!file.exists()) {
            std::cout << "Downloading ULWGL\n";
			downloadfile("https://github.com/Open-Wine-Components/ULWGL-launcher/releases/download/0.1-RC3/ULWGL-launcher.tar.gz");
		}
		nextProcess = {"tar", {"-xvf", confpath + "download/ULWGL-launcher.tar.gz", "-C", ulwglfolder}};
	}
}

void LauncherConfig::on_buttonBox_accepted()
{
    if (gameconf->winefolder != ui->winefEdit->text() ||
        gameconf->protonfolder != ui->protonfEdit->text() ||
            gameconf->dxvkfolder != ui->dxvkfEdit->text() ||
            gameconf->vkd3dfolder != ui->vkd3dfEdit->text() ||
            gameconf->ulwglfolder != ui->ulwglfEdit->text() ||
            enabletray != ui->trayCheckBox->isChecked() ||
            iconsize != ui->chooseIconSize->currentText().toInt() ) {
        gameconf->winefolder = ui->winefEdit->text();
        gameconf->protonfolder = ui->protonfEdit->text();
        gameconf->dxvkfolder = ui->dxvkfEdit->text();
        gameconf->vkd3dfolder = ui->vkd3dfEdit->text();
		gameconf->ulwglfolder = ui->ulwglfEdit->text();
		enabletray = ui->trayCheckBox->isChecked();
		iconsize = ui->chooseIconSize->currentText().toInt();
        std::ofstream file(confpath.toStdString()+"launcher.conf");
        if(!file.good()) {
            std::cerr << "Error saving launcher.conf\n";
            return;
        }
        file << "winefolder=" << gameconf->winefolder.toStdString() << std::endl;
        file << "protonfolder=" << gameconf->protonfolder.toStdString() << std::endl;
        file << "dxvkfolder=" << gameconf->dxvkfolder.toStdString() << std::endl;
        file << "vkd3dfolder=" << gameconf->vkd3dfolder.toStdString() << std::endl;
        file << "ulwglfolder=" << gameconf->ulwglfolder.toStdString() << std::endl;
        if (enabletray) {
            file << "tray=true\n";
            tray.show();
        } else {
            file << "tray=false\n";
            tray.hide();
        }
        file << "iconsize=" << iconsize << std::endl;
        file.close();
    }
    QDir dir(gameconf->winefolder);
    dir.mkpath(gameconf->winefolder);
    dir.mkpath(gameconf->protonfolder);
    dir.mkpath(gameconf->dxvkfolder);
    dir.mkpath(gameconf->vkd3dfolder);
    launcher->setGamelistSize(iconsize);

	checkulwgl(gameconf->ulwglfolder, confpath);

	//  gameconf->readWineDxvk();
	
}

void LauncherConfig::trayClicked(QSystemTrayIcon::ActivationReason reason_) {
    toggleVisible();
}
void LauncherConfig::trayShowHide() {
    toggleVisible();
}
