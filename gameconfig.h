#ifndef GAMECONFIG_H
#define GAMECONFIG_H

#include <QProcess>
#include <QDialog>
#include <QFileDialog>
#include <QDateTime>
#include "qlistwidget.h"

struct env_entry  {
    QString name;
    QString value;
};

class Game {
public:
    explicit Game();
	QString name;
	QString directory;
    QString icon;
    QString exec;
    QString winepfx;
    QString runner;
    QString runnerver;
	QString ulwgl_id;
	QProcess *process;
	QString gamelog;
    QString cmdpfx;
    QString args;
    QString dxvk;
    QString vkd3d;
    std::vector<env_entry> env;
    QDateTime runtime;
	int playhour = 0, playmin = 0, playsec = 0;
};

namespace Ui {
class GameConfig;
}

class GameConfig : public QDialog
{
    Q_OBJECT

public:
    explicit GameConfig(QWidget *parent = nullptr);
    ~GameConfig();

    void readRunnerDxvk(Game *confgame);
    bool Config(Game *confgame);
    void saveGame(Game *game);
    Game *loadGame(std::string name);
    void loadGames(std::vector<Game> *games, QListWidget *gamelist);
    void savePlayed(std::vector<Game> *games);
    void loadPlayed(std::vector<Game> *games);

    //QFileDialog *fod;
    Game *game;
    Game *defaultconf;
    QStringList winever;
    QStringList protonver;
    QStringList dxvkver;
    QStringList vkd3dver;
    QString winefolder;
    QString protonfolder;
    QString dxvkfolder;
    QString vkd3dfolder;
    QString ulwglfolder;

private slots:
    void on_execBrowse_clicked();

    void on_winepfxBrowse_clicked();

    void on_dirBrowse_clicked();

    void on_envAddButton_clicked();

    void on_envDelButton_clicked();

    void on_iconBrowse_clicked();
    void on_runnerChoose_textActivated(const QString &arg1);

    void on_ulwglSearch_clicked();

private:
    Ui::GameConfig *ui;
    QString confpath;
};


#endif // GAMECONFIG_H
