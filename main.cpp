#include "qtlaunch.h"

#include <QApplication>
#include <QCloseEvent>
#include <QSharedMemory>
#include <iostream>
#include <QLocalSocket>
#include <QLocalServer>

QtLaunch *win;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    const QString appKey = "QtLaunchKey";
    QLocalSocket *socket = new QLocalSocket();
    socket->connectToServer(appKey);
    if (socket->isOpen()) {
        socket->close();
        socket->deleteLater();
        return 0;
    }
    socket->deleteLater();

    QtLaunch w;
    win = &w;

    QLocalServer server;
    QObject::connect(&server, &QLocalServer::newConnection, [&w] () {
        /*Set the window on the top level.*/
        w.setWindowFlags(w.windowFlags() | Qt::WindowStaysOnTopHint);
        w.showNormal();
        w.setWindowFlags(w.windowFlags() & ~Qt::WindowStaysOnTopHint);
        w.showNormal();
        w.activateWindow();
    });
    server.listen(appKey);

    w.show();
    int ret = a.exec();
    server.close();
    return ret;
}

void toggleVisible() {
    if (win->isVisible()) {
        win->hide();
    } else {
        win->show();
    }
}

void QWidget::closeEvent(QCloseEvent *e) {
    if(win->launchconf.enabletray) {
		e->ignore();
		win->hide();
    }
}
